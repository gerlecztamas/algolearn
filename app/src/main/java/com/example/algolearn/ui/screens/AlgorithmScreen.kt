package com.example.algolearn.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AlgorithmScreen(
    state: RoomAlgorithmState,
    onEvent: (RoomAlgorithmEvent) -> Unit,
    selectedAlgorithm: String
){
    //val dao: TheoryAlgorithmDao
    //val theoryAlgorithms = dao.getTheoryAlgorithmsOrderedByName()
    var isPractice by remember { mutableStateOf(false) }

    val darkColorString = "#313852"
    val darkColor = Color(android.graphics.Color.parseColor(darkColorString))
    val lightColorString = "#8EBBFF"
    val lightColor = Color(android.graphics.Color.parseColor(lightColorString))
    val backgroundColorToParse = "#24293E"
    val backgroundColor = Color(android.graphics.Color.parseColor(backgroundColorToParse))


    Scaffold () { padding ->
        LazyColumn(
            contentPadding = padding,
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            item {
                Text(text = selectedAlgorithm, color = Color.White)
            }
            if (isPractice) {
                item {
                    Text(text = "Practice", color = Color.White)
                }
            }
            else {
                item {
                    Text(text = "Theory", color = Color.White)
                    DisplayTheory(state, selectedAlgorithm)
                }
            }
            item {
                Row (
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ){
                    Box(
                        modifier = Modifier
                            .background(if (isPractice) lightColor else darkColor)
                            .size(width = if (isPractice) 80.dp else 100.dp, height = 40.dp)
                            .clickable {
                                isPractice = false
                            }
                    ) {
                        Text("Theory",
                            color = Color.White,
                            modifier = Modifier
                                .align(Alignment.Center)
                        )
                    }
                    Box(
                        modifier = Modifier
                            .background(if (isPractice) darkColor else lightColor)
                            .size(width = if (isPractice) 100.dp else 80.dp, height = 40.dp)
                            .clickable {
                                isPractice = true
                            }
                    ) {
                        Text("Practice",
                            color = Color.White,
                            modifier = Modifier
                                .align(Alignment.Center)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayTheory(
    state: RoomAlgorithmState,
    selectedAlgorithm: String
){
    for (algorithmWithStep in state.algorithmsWithSteps) {
        if (algorithmWithStep.algorithm.name == selectedAlgorithm) {
            Text(text = algorithmWithStep.algorithm.name, color = Color.White)
            algorithmWithStep.steps.forEachIndexed { index, step ->
                Text(text = "$index - ${step.description}", color = Color.White)
            }
        }
    }
}