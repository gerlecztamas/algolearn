package com.example.algolearn.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState
import com.example.algolearn.data.dao.RoomAlgorithmDao
import com.example.algolearn.data.entity.RoomAlgorithm
import com.example.algolearn.data.enums.SortType
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

@OptIn(ExperimentalCoroutinesApi::class)
class RoomAlgorithmViewModel(
    private val dao: RoomAlgorithmDao
) : ViewModel(){
    private val _sortType = MutableStateFlow(SortType.NAME)
    private val  _roomAlgorithms = _sortType
        .flatMapLatest { sortType ->
            when(sortType){
                SortType.NAME -> dao.getRoomAlgorithmsOrderedByName()
                SortType.FREESTYLE -> TODO()
            }
        }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

    private val _algorithmsWithLevels = _sortType
        .flatMapLatest { dao.getAlgorithmsWithLevels() }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())

    private val _algorithmsWithSteps = _sortType
        .flatMapLatest { dao.getAlgorithmsWithSteps() }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyList())



    private val _state = MutableStateFlow(RoomAlgorithmState())
    val state = combine(_state, _sortType, _roomAlgorithms, _algorithmsWithLevels, _algorithmsWithSteps) {state, sortType, roomAlgorithms, algorithmsWithLevels, algorithmsWithSteps ->
        state.copy(
            algorithmsWithSteps = algorithmsWithSteps,
            algorithmsWithLevels = algorithmsWithLevels,
            roomAlgorithms = roomAlgorithms,
            sortType = sortType
        )
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), RoomAlgorithmState())

    fun onEvent(event: RoomAlgorithmEvent) {
        when(event) {
            is RoomAlgorithmEvent.DeleteRoomAlgorithm -> {
                viewModelScope.launch {
                    dao.deleteRoomAlgorithm(event.roomAlgorithm)
                }
            }
            RoomAlgorithmEvent.HideDialog -> {
                _state.update { it.copy(
                    isAddingRoomAlgorithm = false
                ) }
            }
            RoomAlgorithmEvent.ShowDialog -> {
                _state.update { it.copy(
                    isAddingRoomAlgorithm = true
                ) }
            }
            is RoomAlgorithmEvent.SortRoomAlgorithms -> {
                _sortType.value = event.sortType
            }
            RoomAlgorithmEvent.saveRoomAlgorithm -> {
                val name = state.value.name
                val freestyle = state.value.freestyle

                if (name.isBlank()) {
                    return
                }

                val roomAlgorithm = RoomAlgorithm(
                    name = name,
                    freestyle = freestyle
                )
                viewModelScope.launch {
                    dao.upsertRoomAlgorithm(roomAlgorithm)
                }
                _state.update { it.copy(
                    isAddingRoomAlgorithm = false,
                    name = "",
                    freestyle = false
                ) }
            }
            is RoomAlgorithmEvent.setFreestyle -> {
                _state.update { it.copy(
                    freestyle = event.freestyle
                ) }
            }
            is RoomAlgorithmEvent.setName -> {
                _state.update { it.copy(
                    name = event.name
                ) }
            }
        }
    }
}