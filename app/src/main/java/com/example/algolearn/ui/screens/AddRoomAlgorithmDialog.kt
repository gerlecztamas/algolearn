package com.example.algolearn.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddRoomAlgorithmDialog (
    state: RoomAlgorithmState,
    onEvent: (RoomAlgorithmEvent) -> Unit,
    modifier: Modifier = Modifier
){
    AlertDialog(
        modifier = modifier,
        onDismissRequest = {
            onEvent(RoomAlgorithmEvent.HideDialog)
        },
        title = { Text(text = "Add RoomAlgorithm")},
        text = {
            Column (
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                TextField(
                    value = state.name,
                    onValueChange = {
                        onEvent(RoomAlgorithmEvent.setName(it))
                    },
                    placeholder = {
                        Text(text = "Name")
                    }
                )
            }
        },
        confirmButton = {
            Box(
                modifier = Modifier.fillMaxWidth(),
                contentAlignment = Alignment.CenterEnd
            ){
                Button(onClick = {
                    onEvent(RoomAlgorithmEvent.saveRoomAlgorithm)
                }) {
                    Text(text = "Save")
                }
            }
        }
    )
}