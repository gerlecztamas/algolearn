
package com.example.algolearn.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState
import com.example.algolearn.data.entity.RoomLevel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProgressScreen(
    state: RoomAlgorithmState,
    onEvent: (RoomAlgorithmEvent) -> Unit
){
    val backgroundColorToParse = "#24293E"
    val backgroundColor = Color(android.graphics.Color.parseColor(backgroundColorToParse))

    Scaffold (

    ) { padding ->
        LazyColumn(
            contentPadding = padding,
            modifier = Modifier
                .background(backgroundColor)
                .fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            item {
                Text(text = "Progress", color = Color.White)
            }
            items(state.algorithmsWithLevels) { algorithmWithLevels ->
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(130.dp)
                    //.border(1.dp, Color.Black, shape = RoundedCornerShape(8.dp))
                    //.background(color = itemBackGroundColor),
                    , horizontalArrangement = Arrangement.Center
                ) {
                    Column(
                        modifier = Modifier
                            .weight(1f)
                    ) {
                        Text(
                            text = "${algorithmWithLevels.algorithm.name}",
                            color = Color.White,
                            fontSize = 20.sp,
                        )
                        /*for (level in algorithmWithLevels.levels) {
                            Text(
                                text = "${level.number}",
                                color = Color.White,
                                fontSize = 20.sp,
                            )
                        }*/
                        DisplayLevels(levels = algorithmWithLevels.levels)
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayLevels(
    levels: List<RoomLevel>
){
    for (level in levels) {
        Text(text = "Level: ${level.number} - Rating: ${level.rating}%", color = Color.White)
    }
}