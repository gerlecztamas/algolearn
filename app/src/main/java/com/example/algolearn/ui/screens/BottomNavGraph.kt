package com.example.algolearn.ui.screens

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState


@Composable
fun BottomNavGraph(navController: NavHostController, state: RoomAlgorithmState,
                   onEvent: (RoomAlgorithmEvent) -> Unit) {
    NavHost(
        navController = navController,
        startDestination = BottomBarScreen.Home.route
    ) {
        composable(route = BottomBarScreen.Home.route) {
            HomeScreen(state = state, onEvent = onEvent, navController = navController)
        }
        composable(route = "algorithm") {
            AlgorithmScreen(selectedAlgorithm = state.selectedName, state = state, onEvent = onEvent, /*navController = navController*/)
        }
        composable(route = BottomBarScreen.Progress.route) {
            ProgressScreen(state, onEvent)
        }
        composable(route = BottomBarScreen.Settings.route) {
            //SettingsScreen()
        }
    }
}