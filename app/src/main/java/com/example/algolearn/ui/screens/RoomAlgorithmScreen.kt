package com.example.algolearn.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState
import com.example.algolearn.data.enums.SortType

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RoomAlgorithmScreen(
    state: RoomAlgorithmState,
    onEvent: (RoomAlgorithmEvent) -> Unit
){

    Scaffold (
        /*floatingActionButton = {
            FloatingActionButton(onClick = {
                onEvent(RoomAlgorithmEvent.ShowDialog)
            }) {
                Icon(imageVector = Icons.Default.Add,
                    contentDescription = "Add"
                )
            }
        },
        modifier = Modifier.padding(16.dp)*/
        /*bottomBar = {
            Row(
                modifier = Modifier
                    .height(109.dp)
                    .fillMaxWidth()
                    .background(
                        color =menuColor
                    )

            ){
                CenteredIcon(Icons.Default.Home)
                CenteredIcon(Icons.Default.Home)
                CenteredIcon(Icons.Default.Home)
            }
        }*/

    ) { padding ->
        if (state.isAddingRoomAlgorithm) {
            AddRoomAlgorithmDialog(state = state, onEvent = onEvent)
        }
            LazyColumn(
                contentPadding = padding,
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                item {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .horizontalScroll(rememberScrollState()),
                        verticalAlignment = CenterVertically
                    ) {
                        SortType.values().forEach { sortType ->
                            Row(
                                modifier = Modifier
                                    .clickable {
                                        onEvent(RoomAlgorithmEvent.SortRoomAlgorithms(sortType))
                                    },
                                verticalAlignment = CenterVertically
                            ) {
                                RadioButton(
                                    selected = state.sortType == sortType,
                                    onClick = {
                                        onEvent(RoomAlgorithmEvent.SortRoomAlgorithms(sortType))
                                    }
                                )
                                Text(text=sortType.name)
                            }
                        }
                    }
                }
                items(state.roomAlgorithms) { roomAlgorithm ->
                    Row (
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Column (
                            modifier = Modifier.weight(1f)
                        ){
                            Text(
                                text = "${roomAlgorithm.name} ${roomAlgorithm.freestyle}",
                                fontSize = 20.sp
                            )
                        }
                        IconButton(onClick = {
                            onEvent(RoomAlgorithmEvent.DeleteRoomAlgorithm(roomAlgorithm))
                        }) {
                            Icon(
                                imageVector = Icons.Default.Delete,
                                contentDescription = "Delete"
                            )
                        }
                    }
                }
            }
        }
}

