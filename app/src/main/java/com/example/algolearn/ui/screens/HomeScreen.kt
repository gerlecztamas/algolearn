package com.example.algolearn.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import com.example.algolearn.RoomAlgorithmEvent
import com.example.algolearn.RoomAlgorithmState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    state: RoomAlgorithmState,
    onEvent: (RoomAlgorithmEvent) -> Unit,
    navController: NavController
){
    val backgroundColorToParse = "#24293E"
    val itemBackgroundColorToParse = "#8EBBFF"

    val backgroundColor = Color(android.graphics.Color.parseColor(backgroundColorToParse))
    val itemBackGroundColor = Color(android.graphics.Color.parseColor(itemBackgroundColorToParse))

    Scaffold (

    ) { padding ->
        LazyColumn(
            contentPadding = padding,
            modifier = Modifier
                .fillMaxSize()
                .background(color = backgroundColor),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            items(state.roomAlgorithms) { roomAlgorithm ->
                Row (
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(30.dp)
                        //.border(1.dp, Color.Black, shape = RoundedCornerShape(8.dp))
                        .background(color = itemBackGroundColor),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Column (
                        modifier = Modifier
                            .weight(1f)
                    ){
                        Text(
                            text = "${roomAlgorithm.name}",
                            color = Color.White,
                            fontSize = 20.sp,
                            modifier = Modifier
                                .clickable {
                                    state.selectedName = roomAlgorithm.name
                                    navController.navigate("algorithm") {
                                        popUpTo(navController.graph.findStartDestination().id)
                                        launchSingleTop = true
                                    }
                                }
                        )
                    }
                }
            }
        }
    }
}