package com.example.algolearn

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.algolearn.data.RoomAlgorithmDatabase
import com.example.algolearn.ui.screens.MainScreen
import com.example.algolearn.ui.theme.AlgoLearnTheme
import com.example.algolearn.ui.viewmodels.RoomAlgorithmViewModel


class MainActivity : ComponentActivity() {
    /*lateinit var graphView: GraphView
    lateinit var next : String
    //val miArreglo = Array(20) { Step() }
    var asd : Int = 0
    /*var eye  = findViewById<TextView>(R.id.text_i)
    var blah = findViewById<TextView>(R.id.text_blah)
    var boo = findViewById<TextView>(R.id.text_boolean)*/
    lateinit var eye : TextView
    lateinit var blah : TextView
    lateinit var boo : TextView
    private lateinit var button: TextView;*/

    private val algoLearnDB by lazy {
        /*Room.databaseBuilder(
            applicationContext,
            RoomAlgorithmDatabase::class.java,
            "test.db"
        ).build()*/
        Room.databaseBuilder(applicationContext, RoomAlgorithmDatabase::class.java, "algo_learn.db")
            .createFromAsset("algolearn.db")
            .build()

    }

    private val viewModel by viewModels<RoomAlgorithmViewModel>(
        factoryProducer = {
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return RoomAlgorithmViewModel(algoLearnDB.dao) as T
                }
            }
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*setContentView(R.layout.activity_main)
        val homeButton : ImageButton = findViewById<ImageButton>(R.id.home_button)
        val progressButton : ImageButton = findViewById<ImageButton>(R.id.progress_button)
        homeButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        progressButton.setOnClickListener{
            val intent = Intent(this, Progress::class.java)
            startActivity(intent)
        }*/
        /*setContent {
            AlgoLearnTheme {
                val state by viewModel.state.collectAsState()
                RoomAlgorithmScreen(state = state, onEvent = viewModel::onEvent)
            }
        }*/
        setContent {
            AlgoLearnTheme {
                val state by viewModel.state.collectAsState()
                MainScreen(state = state, onEvent = viewModel::onEvent)
            }
        }
    }


    @Composable
    fun Greeting(name: String, modifier: Modifier = Modifier) {

        if (5 > 6) {
            Text(
                text = "Hello $name!",
                modifier = modifier
            )
        } else {
            Text(
                text = "Az baj",
                modifier = modifier
            )
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun GreetingPreview() {
        AlgoLearnTheme {
            Greeting("Android")
        }
    }
}