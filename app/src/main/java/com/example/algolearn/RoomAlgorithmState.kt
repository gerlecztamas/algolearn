package com.example.algolearn

import com.example.algolearn.data.dao.AlgorithmWithLevels
import com.example.algolearn.data.dao.AlgorithmWithSteps
import com.example.algolearn.data.entity.RoomAlgorithm
import com.example.algolearn.data.enums.SortType

data class RoomAlgorithmState(
    val roomAlgorithms: List<RoomAlgorithm> = emptyList(),
    val algorithmsWithLevels: List<AlgorithmWithLevels> = emptyList(),
    val algorithmsWithSteps: List<AlgorithmWithSteps> = emptyList(),
    val name: String = "",
    val freestyle: Boolean = false,
    val isAddingRoomAlgorithm: Boolean = false,
    val sortType: SortType = SortType.NAME,
    var selectedName: String = ""
)
