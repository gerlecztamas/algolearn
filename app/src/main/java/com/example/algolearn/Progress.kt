package com.example.algolearn

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton

class Progress : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress)
        val homeButton : ImageButton = findViewById<ImageButton>(R.id.home_button)
        val progressButton : ImageButton = findViewById<ImageButton>(R.id.progress_button)
        homeButton.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        progressButton.setOnClickListener{
            val intent = Intent(this, Progress::class.java)
            startActivity(intent)
        }
    }
}