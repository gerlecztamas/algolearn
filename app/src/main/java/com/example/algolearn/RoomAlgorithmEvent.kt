package com.example.algolearn

import com.example.algolearn.data.entity.RoomAlgorithm
import com.example.algolearn.data.enums.SortType

sealed interface RoomAlgorithmEvent{
    object saveRoomAlgorithm: RoomAlgorithmEvent
    data class setName(val name: String): RoomAlgorithmEvent
    data class setFreestyle(val freestyle: Boolean): RoomAlgorithmEvent
    object ShowDialog: RoomAlgorithmEvent
    object HideDialog: RoomAlgorithmEvent
    data class SortRoomAlgorithms(val sortType: SortType): RoomAlgorithmEvent
    data class DeleteRoomAlgorithm(val roomAlgorithm: RoomAlgorithm): RoomAlgorithmEvent
}