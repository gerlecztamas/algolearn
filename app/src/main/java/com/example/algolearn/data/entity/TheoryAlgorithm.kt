package com.example.algolearn.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity("theory_algorithm")
data class TheoryAlgorithm(
    val name: String,
    val description: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null
)