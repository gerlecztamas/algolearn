package com.example.algolearn.data.enums

enum class SortType {
    NAME,
    FREESTYLE
}