package com.example.algolearn.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.algolearn.data.dao.RoomAlgorithmDao
import com.example.algolearn.data.entity.RoomAlgorithm
import com.example.algolearn.data.entity.RoomLevel
import com.example.algolearn.data.entity.TheoryAlgorithm
import com.example.algolearn.data.entity.TheoryStep

@Database(
    entities = [RoomAlgorithm::class, TheoryAlgorithm::class, RoomLevel::class, TheoryStep::class],
    version = 1
)
abstract class RoomAlgorithmDatabase: RoomDatabase() {
    abstract val dao: RoomAlgorithmDao
}