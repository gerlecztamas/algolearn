package com.example.algolearn.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity("algorithm")
data class RoomAlgorithm (

    val name: String,
    val freestyle: Boolean,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null
)