package com.example.algolearn.data.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "step",
    foreignKeys = [ForeignKey(
        entity = TheoryAlgorithm::class,
        childColumns = ["algorithmId"],
        parentColumns = ["id"]
    )]
)
data class TheoryStep (
    val description: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val algorithmId: Int
)