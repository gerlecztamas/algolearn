package com.example.algolearn.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Embedded
import androidx.room.Query
import androidx.room.Relation
import androidx.room.Transaction
import androidx.room.Upsert
import com.example.algolearn.data.entity.RoomAlgorithm
import com.example.algolearn.data.entity.RoomLevel
import com.example.algolearn.data.entity.TheoryAlgorithm
import com.example.algolearn.data.entity.TheoryStep
import kotlinx.coroutines.flow.Flow

data class AlgorithmWithLevels(
    @Embedded val algorithm: RoomAlgorithm,
    @Relation(
        parentColumn = "id",
        entityColumn = "algorithmId"
    )
    val levels: List<RoomLevel>
)

data class AlgorithmWithSteps(
    @Embedded val algorithm: TheoryAlgorithm,
    @Relation(
        parentColumn = "id",
        entityColumn = "algorithmId"
    )
    val steps: List<TheoryStep>
)

@Dao
interface RoomAlgorithmDao {
    @Upsert()
    suspend fun upsertRoomAlgorithm(roomAlgorithm: RoomAlgorithm)

    @Delete
    suspend fun deleteRoomAlgorithm(roomAlgorithm: RoomAlgorithm)

    @Query("SELECT * FROM algorithm ORDER BY name asc")
    fun getRoomAlgorithmsOrderedByName(): Flow<List<RoomAlgorithm>>

    @Query("SELECT * FROM theory_algorithm")
    fun getTheoryAlgorithms(): Flow<List<TheoryAlgorithm>>

    @Transaction
    @Query("SELECT * FROM algorithm")
    fun getAlgorithmsWithLevels(): Flow<List<AlgorithmWithLevels>>

    @Transaction
    @Query("SELECT * FROM theory_algorithm")
    fun getAlgorithmsWithSteps(): Flow<List<AlgorithmWithSteps>>
}