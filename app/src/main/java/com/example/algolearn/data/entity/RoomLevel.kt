package com.example.algolearn.data.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "level",
    foreignKeys = [ForeignKey(
        entity = RoomAlgorithm::class,
        childColumns = ["algorithmId"],
        parentColumns = ["id"]
    )]
    )
data class RoomLevel (
    val rating: Int,
    val number: Int,
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val algorithmId: Int
)