package com.graphdraw;

import com.graphdraw.model.Node;

public interface OnNodeClickListener {
    void onClick(GraphView view, Node node);
}
